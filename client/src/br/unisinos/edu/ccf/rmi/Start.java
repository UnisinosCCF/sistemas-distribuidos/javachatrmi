package br.unisinos.edu.ccf.rmi;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import br.unisinos.edu.ccf.rmi.client.Client;
import br.unisinos.edu.ccf.rmi.server.Server;

/**
 * Classe de inicializa��o do sistema.
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 27/08/2020
 */
public class Start {

	public static void main(String[] args) throws RemoteException, MalformedURLException, NotBoundException {

		if (args == null) {
			System.out.println("Falta ser informado o parametro 1 que define o tipo de instancia. informe 'S' ou 'C'");
			System.exit(0);
		}
		if (args.length < 2) {
			System.out.println("Falta ser informado o parametro 2 que define o nome do servidor ou do cliente.");
			System.exit(0);
		} else {
			String serverName = null;
			String serverHost;
			int port = 0;
			if (args[0].equalsIgnoreCase("S")) {

				if (args[1].length() >= 1) {
					serverName = args[1];
				} else {
					System.out.println("O segundo parametro deve ser o nome para o servico ex: 'servidorChat'");
					System.exit(0);
				}

				if (args.length >= 3) {
					if (args[2].length() >= 1) {
						serverHost = args[2];
						// Inicia o servico
						new Server(serverName, serverHost).run();
					} else {
						System.out.println("O terceiro parametro deve ser o host para o servico ex: 'localhost' ou '10.0.0.121'");
						System.exit(0);
					}

				} else {
					new Server(serverName).run();
				}

			} else if (args[0].equalsIgnoreCase("C")) {

				String clientName = null;
				if (args[1].length() >= 1) {
					clientName = args[1];
				} else {
					System.out.println("O segundo parametro deve ser o nome para o cliente ex: 'Fulano de tal'");
					System.exit(0);
				}

				if (args.length >= 3) {
					if (args[2].length() >= 1) {

						if (args[2].length() >= 1) {
							serverName = args[2];
						} else {
							System.out.println("O terceiro parametro deve ser o nome do servidor que sera conectado ex: 'servidorChat'");
							System.exit(0);
						}

						try {
							port = Integer.parseInt(args[3]);
						} catch (Exception e) {
							System.out.println("O quarto parametro deve ser a porta para conex�o com o servidor em numero inteiro ex: '1589' ou '15897'");
							System.exit(0);
						}

						if (args.length >= 5) {
							if (args[4].length() >= 1) {
								serverHost = args[4];
								// Inicia o servico
								new Client(clientName, serverName, port, serverHost).run();
								System.exit(0);
							} else {
								System.out.println("O quinto parametro deve ser o host para o servico ex: 'localhost' ou '10.0.0.121'");
								System.exit(0);
							}
						} else {
							new Client(clientName, serverName, port).run();
							System.exit(0);
						}

					} else {
						System.out.println("O quarto parametro deve ser a porta para conex�o com o servidor em numero inteiro ex: '1589' ou '15897'");
						System.exit(0);
					}

				}
				System.out.println("O terceiro parametro deve ser o nome do servidor para conectar ex: 'servidorChat'");
				System.exit(0);
			} else {
				System.out.println("O primeiro parametro deve ser 'S' para servidor ou 'C' para cliente");
				System.exit(0);
			}

		}
	}
}
