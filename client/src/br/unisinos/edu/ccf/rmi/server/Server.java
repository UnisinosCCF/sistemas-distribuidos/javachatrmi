package br.unisinos.edu.ccf.rmi.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.edu.ccf.rmi.Util;
import br.unisinos.edu.ccf.rmi.client.ChatClient;
import br.unisinos.edu.ccf.rmi.client.Client;

/**
 * Classe que representa o servidor.
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 27/08/2020
 */
public class Server extends UnicastRemoteObject implements ChatServer {

	private List<ChatClient> connections;
	private static int server_port = 15989;
	private static long client_unique_id = 3500;
	private String serverName;
	private String serverHost = "localhost";

	public Server(String serverName) throws RemoteException {
		this.serverName = serverName;
		this.connections = new ArrayList<ChatClient>();
	}

	public Server(String serverName, String serverHost) throws RemoteException {
		this.serverName = serverName;
		this.serverHost = serverHost;
		this.connections = new ArrayList<ChatClient>();
	}

	@Override
	public String conectar(ChatClient c) throws RemoteException {
		c.setIdentificador(client_unique_id++);
		connections.add(c);
		String men = c.name() + " (" + c.getIdentificador() + ") Conectado no chat.";
		System.out.println(men);
		enviarServer(men);
		return ("Usuario Conectado");
	}

	/**
	 * Metodo de verifica��o de de clientes.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 27/08/2020
	 * @param c
	 * @return
	 * @throws RemoteException
	 */
	private boolean verificarCliente(ChatClient c) throws RemoteException {
		for (int i = 0; i < connections.size(); i++) {
			if (connections.get(i).getIdentificador() == c.getIdentificador()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void enviar(ChatClient c, String mensagem) throws RemoteException {
		if (verificarCliente(c)) {
			for (int i = 0; i < connections.size(); i++) {
				if (connections.get(i).getIdentificador() != c.getIdentificador()) {
					connections.get(i).exibir(c.name() + " (" + c.getIdentificador() + ") : " + mensagem);
				}
			}
		}
	}

	/**
	 * Envia mensagem para todos os clientes conectados.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 27/08/2020
	 * @param mensagem
	 * @throws RemoteException
	 */
	public void enviarServer(String mensagem) throws RemoteException {
		for (int i = 0; i < connections.size(); i++) {
			connections.get(i).exibir("Servidor (info) : " + mensagem);
		}
	}

	@Override
	public void desconectar(ChatClient c) throws RemoteException {
		connections.remove(c);
		String men = c.name() + " (" + c.getIdentificador() + ") Desconectado no chat.";
		System.out.println(men);
		enviarServer(men);
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	/**
	 * Monta a URL.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 27/08/2020
	 * @param port
	 * @return
	 */
	private String getUrl(int port) {
		return "//" + this.getServerHost() + ":" + port + "/" + this.getServerName();
	}

	/**
	 * Inicia o servico.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 27/08/2020
	 * @throws RemoteException
	 * @throws MalformedURLException
	 */
	public void run() throws RemoteException, MalformedURLException {

		// Verifica uma porta disponivel
		boolean free = false;
		int port = 0;
		while (!free && server_port < 18000) {
			port = server_port++;
			free = Util.checkPort(port);
		}

		if (!free) {
			System.out.println("Impossivel utilizar uma porta para o servidor.");
			System.exit(0);
		}
		LocateRegistry.createRegistry(port);
		ChatServer s = this;
		Naming.rebind(this.getUrl(port), s);
		System.out.println("Desenvolvido por Cristiano Farias <cristiano@ccfdeveloper.com> \n");
		System.out.println("Servidor de chat RMI online. Acesso o cliente atrav�s do endere�o: " + this.getUrl(port));
		System.out.println("");
	}

}