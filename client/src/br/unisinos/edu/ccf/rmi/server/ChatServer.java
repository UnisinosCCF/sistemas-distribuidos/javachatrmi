package br.unisinos.edu.ccf.rmi.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import br.unisinos.edu.ccf.rmi.client.ChatClient;

public interface ChatServer extends Remote {
	public String conectar(ChatClient c) throws RemoteException;

	public void enviar(ChatClient c, String mensagem) throws RemoteException;

	public void desconectar(ChatClient c) throws RemoteException;
}
