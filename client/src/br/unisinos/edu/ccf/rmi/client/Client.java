package br.unisinos.edu.ccf.rmi.client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

import br.unisinos.edu.ccf.rmi.server.ChatServer;

public class Client extends UnicastRemoteObject implements ChatClient {

	private long identificador;
	private String name;
	private int serverPort;
	private String serverName;
	private String serverHost = "localhost";

	public Client(String name, String serverName, int serverPort) throws RemoteException {
		this.name = name;
		this.serverName = serverName;
		this.serverPort = serverPort;
	}

	public Client(String name, String serverName, int serverPort, String serverHost) throws RemoteException {
		this.name = name;
		this.serverHost = serverHost;
		this.serverName = serverName;
		this.serverPort = serverPort;
	}

	@Override
	public void exibir(String mensagem) throws RemoteException {
		try {
			System.out.println(mensagem);
			System.out.print(" >>> ");
		} catch (Exception e) {
			System.out.println("Erro" + e);
		}

	}

	@Override
	public void setIdentificador(long uniqueID) throws RemoteException {
		this.identificador = uniqueID;
	}

	@Override
	public long getIdentificador() throws RemoteException {
		return this.identificador;
	}

	@Override
	public String name() throws RemoteException {
		return this.name;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	private String getUrl() {
		return "//" + this.getServerHost() + ":" + this.getServerPort() + "/" + this.getServerName();
	}

	private void write(ChatServer server) throws RemoteException {

		String line;
		boolean exit = false;
		while (!exit) {
			Scanner ler = new Scanner(System.in);
			// Ler teclado.
			System.out.print(" >>> ");
			line = ler.nextLine();
			if (line != null) {
				if (line.equalsIgnoreCase("sair")) {
					System.out.println("saindo do chat...");
					break;
				}
				server.enviar(this, line);
			}
		}

	}

	public void run() throws MalformedURLException, RemoteException, NotBoundException {

		ChatServer server = (ChatServer) Naming.lookup(getUrl());

		// connect
		server.conectar(this);

		// Escrever
		this.write(server);

		// sair
		server.desconectar(this);
	}

}
