package br.unisinos.edu.ccf.rmi.client;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Cliente que recebera a mensagem do servidor.
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 26/08/2020
 */
public interface ChatClient extends Remote {

	public void exibir(String mensagem) throws RemoteException;

	public void setIdentificador(long uniqueID) throws RemoteException;

	public long getIdentificador() throws RemoteException;

	public String name() throws RemoteException;

}
