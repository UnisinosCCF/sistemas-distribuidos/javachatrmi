package br.unisinos.edu.ccf.rmi;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Classe utilitaria
 * 
 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
 * @since 26/08/2020
 */
public class Util {

	/**
	 * Metodo que verifica a disponibilidade de porta.
	 * 
	 * @author Cristiano Farias <cristiano@ccfdeveloper.com>
	 * @since 26/08/2020
	 * @param port
	 * @return
	 */
	public static boolean checkPort(int port) {
		boolean portTaken = true;
		ServerSocket socket = null;
		System.out.println("Checando.. " + port);

		try {
			socket = new ServerSocket(port);
		} catch (IOException e) {
			portTaken = false;
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return portTaken;
	}
}
